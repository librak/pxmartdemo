package com.homework.pxmartdemo.api.gson


data class QRCode (
    var qr_code: String? = null
)