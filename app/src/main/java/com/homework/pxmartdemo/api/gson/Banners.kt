package com.homework.pxmartdemo.api.gson


data class Banners (
    var banners: List<Banner>? = null
)

data class Banner (
    var title: String? = null,
    var image: String? = null,
    var target_url: String? = null
)