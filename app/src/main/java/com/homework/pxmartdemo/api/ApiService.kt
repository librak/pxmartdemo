package com.homework.pxmartdemo.api

import com.homework.pxmartdemo.api.gson.Banners
import com.homework.pxmartdemo.api.gson.Messages
import com.homework.pxmartdemo.api.gson.QRCode
import retrofit2.Call
import retrofit2.http.*

interface ApiService {
    @Headers("Content-Type: application/json")
    @GET("/v3/f6733f2d-82fc-43e7-b19d-d8381f0ab91e")
    fun getBanners(): Call<PXResponse<Banners>>

    @Headers("Content-Type: application/json")
    @GET("/v3/0f0488e1-e532-45e5-8033-bef5904359fe")
    fun getMessages(): Call<PXResponse<Messages>>

    @Headers("Content-Type: application/json")
    @GET("/v3/8c29aeec-3ab4-4ac1-9b2e-e99652dbd155")
    fun getPayment (): Call<PXResponse<QRCode>>

}