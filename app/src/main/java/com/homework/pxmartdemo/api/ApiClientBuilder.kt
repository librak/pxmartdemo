package com.homework.pxmartdemo.api

import com.homework.pxmartdemo.Constants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiClientBuilder {
    private const val CONNECTION_TIMEOUT = 15
    private const val SOCKET_TIMEOUT = 15

    fun createApiClient(): ApiService {
        val client = getClient()

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.DOMAIN)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .build()
        return retrofit.create(ApiService::class.java)
    }

    private fun getClient(): OkHttpClient {
        return OkHttpClient.Builder().apply {
            connectTimeout(CONNECTION_TIMEOUT.toLong(), TimeUnit.SECONDS)
            readTimeout(SOCKET_TIMEOUT.toLong(), TimeUnit.SECONDS)
            writeTimeout(SOCKET_TIMEOUT.toLong(), TimeUnit.SECONDS)
        }.build()
    }

}
