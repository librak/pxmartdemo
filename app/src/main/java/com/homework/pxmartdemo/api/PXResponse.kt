package com.homework.pxmartdemo.api

class PXResponse<T> {
    var status_code: Int? = null
    var result: T? = null
}