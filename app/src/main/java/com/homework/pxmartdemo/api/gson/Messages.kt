package com.homework.pxmartdemo.api.gson


data class Messages (var messages: List<Message>? = null)

data class Message (
    var title: String? = null,
    var msg: String? = null,
    var ts: Long = 0L,
    var selected: Boolean = false // not from server
)