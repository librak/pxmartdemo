package com.homework.pxmartdemo

import android.content.res.Resources
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.*

object Utility {
    fun getDateByTime(time: Long): String {
        val sdf = SimpleDateFormat("yyyy.MM.dd hh:mm", Locale.getDefault())
        val date = Date(time)
        return sdf.format(date)
    }

    fun convertGsonToString(gsonObj: Any?): String {
        val gson = Gson()
        return gson.toJson(gsonObj)
    }

    val Int.dp: Int
        get() = (this * Resources.getSystem().displayMetrics.density + 0.5f).toInt()
}