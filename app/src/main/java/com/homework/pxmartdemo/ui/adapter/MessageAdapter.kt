package com.homework.pxmartdemo.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.homework.pxmartdemo.R
import com.homework.pxmartdemo.Utility
import com.homework.pxmartdemo.Utility.dp
import com.homework.pxmartdemo.api.gson.Message
import com.homework.pxmartdemo.ui.MessageActivityViewModel


class MessageAdapter (private val act: AppCompatActivity) : RecyclerView.Adapter<MessageAdapter.ViewHolder>() {
    private val messages = ArrayList<Message>()
    private val viewModel = ViewModelProvider(act).get(MessageActivityViewModel::class.java)

    fun refresh(list: List<Message>) {
        messages.clear()
        messages.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.item_message, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(messages[position])
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val clMessage: ConstraintLayout = itemView.findViewById(R.id.cl_message)
        val cbDelete: CheckBox = itemView.findViewById(R.id.cb_delete)
        val title: TextView = itemView.findViewById(R.id.title)
        val date: TextView = itemView.findViewById(R.id.date)
        val content: TextView = itemView.findViewById(R.id.content)

        @Suppress("DEPRECATION")
        fun bind(message: Message) {
            if (viewModel.selectModel) {
                cbDelete.visibility = View.VISIBLE
                val params = clMessage.layoutParams as ConstraintLayout.LayoutParams
                params.marginEnd = 0.dp
                clMessage.layoutParams = params
            } else {
                cbDelete.visibility = View.GONE
                val params = clMessage.layoutParams as ConstraintLayout.LayoutParams
                params.marginEnd = 32.dp
                clMessage.layoutParams = params
            }
            cbDelete.isChecked = message.selected
            cbDelete.setOnClickListener {
                message.selected = !message.selected
            }


            title.text = message.title
            date.text = Utility.getDateByTime(message.ts)
            content.text = message.msg
        }

    }

}