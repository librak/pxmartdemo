package com.homework.pxmartdemo.ui

import android.annotation.SuppressLint
import android.graphics.*
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.*
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.WriterException
import com.google.zxing.qrcode.QRCodeWriter
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import com.homework.pxmartdemo.R
import com.homework.pxmartdemo.Utility
import com.homework.pxmartdemo.api.ApiClientBuilder
import com.homework.pxmartdemo.api.PXResponse
import com.homework.pxmartdemo.api.gson.QRCode
import com.homework.pxmartdemo.databinding.ActivityQrcodeBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class QRCodeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityQrcodeBinding
    private lateinit var viewModel: QRCodeActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityQrcodeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()
        viewModel = ViewModelProvider(this).get(QRCodeActivityViewModel::class.java)

        setActionBar()
        setView()

        viewModel.httpQRCode()
    }

    @SuppressLint("InflateParams")
    private fun setActionBar() {
        setSupportActionBar(binding.toolbar)
        val customView =
            LayoutInflater.from(this).inflate(R.layout.actionbar_back_title_edit, null)
        val params = ActionBar.LayoutParams(
            ActionBar.LayoutParams.MATCH_PARENT,
            ActionBar.LayoutParams.WRAP_CONTENT,
            Gravity.START or Gravity.CENTER_VERTICAL
        )
        supportActionBar?.run {
            setCustomView(customView, params)
            setDisplayHomeAsUpEnabled(false)
            setDisplayShowHomeEnabled(false)
            setDisplayShowCustomEnabled(true)
            val tvTitle = customView.findViewById<TextView?>(R.id.tv_title)
            tvTitle?.text = getString(R.string.qrcode)
            val btBack = customView.findViewById<ImageButton?>(R.id.btn_back)
            btBack?.setOnClickListener {
                onBackPressed()
            }
            val btEdit = customView.findViewById<TextView?>(R.id.btn_edit)
            btEdit.visibility = View.GONE
        }
    }

    private fun setView() {
        binding.progress.visibility = View.VISIBLE

        viewModel.getLiveQRCode().observe(this, {
            it?.qr_code?.let { qrcode ->
                generateQRCodeImg(qrcode)
            } ?: kotlin.run {
                binding.progress.visibility = View.GONE
                Toast.makeText(this, R.string.qrcode_fail, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun generateQRCodeImg(qrcode: String) {
        CoroutineScope(Dispatchers.Default).launch {
            val writer = QRCodeWriter()
            val hints: MutableMap<EncodeHintType, Any?> = EnumMap(com.google.zxing.EncodeHintType::class.java)
            hints[EncodeHintType.ERROR_CORRECTION] = ErrorCorrectionLevel.H
            try {
                val bitMatrix = writer.encode(qrcode, BarcodeFormat.QR_CODE, 512, 512, hints)
                val width = bitMatrix.width
                val height = bitMatrix.height
                val bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
                for (x in 0 until width) {
                    for (y in 0 until height) {
                        bmp.setPixel(x, y, if (bitMatrix[x, y]) Color.BLACK else Color.WHITE)
                    }
                }
                val canvas = Canvas(bmp)
                canvas.drawBitmap(bmp, Matrix(), null)
                //放入icon圖示
                var icon = BitmapFactory.decodeResource(resources, R.drawable.logo_pxpay_square)
                icon = Bitmap.createScaledBitmap(icon!!, 100, 100, false)
                canvas.drawBitmap(
                    icon,
                    ((bmp.width - icon.width) / 2).toFloat(),
                    ((bmp.height - icon.height) / 2).toFloat(),
                    null
                )

                withContext(Dispatchers.Main) {
                    binding.ivQRCode.setImageBitmap(bmp)

                    binding.progress.visibility = View.GONE
                }
            } catch (e: WriterException) {
                e.printStackTrace()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.hold, R.anim.right_out)
    }
}

class QRCodeActivityViewModel : ViewModel() {
    private val rep = QRCodeActivityRep()
    private var liveQRCode: MutableLiveData<QRCode> = MutableLiveData()

    fun httpQRCode() {
        rep.getPayment(liveQRCode)
    }

    fun getLiveQRCode(): LiveData<QRCode> {
        return liveQRCode
    }

}

class QRCodeActivityRep {
    fun getPayment(liveQRCode: MutableLiveData<QRCode>) {
        val call = ApiClientBuilder.createApiClient().getPayment()
        call.enqueue(object : Callback<PXResponse<QRCode>> {
            override fun onResponse(
                call: Call<PXResponse<QRCode>>,
                response: Response<PXResponse<QRCode>>
            ) {
                if (response.isSuccessful) {
                    Log.i(
                        "QRCodeActivity",
                        "QRCode:${Utility.convertGsonToString(response.body())}"
                    )
                    liveQRCode.postValue(response.body()?.result)
                } else {
                    liveQRCode.postValue(null)
                }
            }

            override fun onFailure(call: Call<PXResponse<QRCode>>, t: Throwable) {
                liveQRCode.postValue(null)
            }

        })
    }
}