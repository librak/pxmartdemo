package com.homework.pxmartdemo.ui

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.homework.pxmartdemo.R
import com.homework.pxmartdemo.Utility
import com.homework.pxmartdemo.api.ApiClientBuilder
import com.homework.pxmartdemo.api.PXResponse
import com.homework.pxmartdemo.api.gson.Message
import com.homework.pxmartdemo.api.gson.Messages
import com.homework.pxmartdemo.databinding.ActivityMessageBinding
import com.homework.pxmartdemo.ui.adapter.MessageAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MessageActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMessageBinding
    private lateinit var viewModel: MessageActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMessageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()
        viewModel = ViewModelProvider(this).get(MessageActivityViewModel::class.java)

        setActionBar()
        setView()

        viewModel.httpMessages()
    }

    @SuppressLint("InflateParams")
    private fun setActionBar() {
        setSupportActionBar(binding.toolbar)
        val customView =
            LayoutInflater.from(this).inflate(R.layout.actionbar_back_title_edit, null)
        val params = ActionBar.LayoutParams(
            ActionBar.LayoutParams.MATCH_PARENT,
            ActionBar.LayoutParams.WRAP_CONTENT,
            Gravity.START or Gravity.CENTER_VERTICAL
        )
        supportActionBar?.run {
            setCustomView(customView, params)
            setDisplayHomeAsUpEnabled(false)
            setDisplayShowHomeEnabled(false)
            setDisplayShowCustomEnabled(true)
            val tvTitle = customView.findViewById<TextView?>(R.id.tv_title)
            tvTitle?.text = getString(R.string.my_message)
            val btBack = customView.findViewById<ImageButton?>(R.id.btn_back)
            btBack?.setOnClickListener {
                onBackPressed()
            }
            val btEdit = customView.findViewById<TextView?>(R.id.btn_edit)
            btEdit?.setOnClickListener {
                if (!viewModel.msgEmpty()) {
                    binding.clDelete.visibility = View.VISIBLE

                    viewModel.selectModel = true

                    binding.rv.adapter?.notifyDataSetChanged()
                }
            }
        }
    }

    private fun setView() {
        binding.progress.visibility = View.VISIBLE

        binding.rv.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val adapter = MessageAdapter(this)
        binding.rv.adapter = adapter

        viewModel.getLiveMessages().observe(this, {
            binding.progress.visibility = View.GONE

            if (it != null) {
                adapter.refresh(it)
                if (it.isEmpty()) {
                    binding.empty.visibility = View.VISIBLE
                } else {
                    binding.empty.visibility = View.GONE
                }
            } else {
                binding.empty.visibility = View.VISIBLE
                Toast.makeText(this, R.string.msg_fail, Toast.LENGTH_SHORT).show()
            }
        })

        binding.btnDelete.setOnClickListener {
            if (viewModel.hasSelectThings()) {
                binding.clDelete.visibility = View.GONE

                viewModel.delete()

                viewModel.falseSelectModel()
            }
        }

        binding.cbDeleteAll.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                viewModel.selectAll()
            } else {
                viewModel.unSelectAll()
            }
            binding.rv.adapter?.notifyDataSetChanged()
        }
    }

    override fun onBackPressed() {
        if (viewModel.selectModel) {
            binding.clDelete.visibility = View.GONE

            viewModel.falseSelectModel()

            binding.rv.adapter?.notifyDataSetChanged()
        } else {
            super.onBackPressed()
            overridePendingTransition(R.anim.hold, R.anim.right_out)
        }
    }
}

class MessageActivityViewModel : ViewModel() {
    private val rep = MessageActivityRep()
    private var liveMessages: MutableLiveData<List<Message>> = MutableLiveData()
    var selectModel = false

    fun getLiveMessages(): LiveData<List<Message>> {
        return liveMessages
    }

    fun httpMessages() {
        rep.getMessage(liveMessages)
    }

    fun delete() {
        val tempList = ArrayList<Message>()
        val list = liveMessages.value
        list?.run {
            for (message in this) {
                if (!message.selected) {
                    tempList.add(message)
                }
            }
        }
        tempList.sortByDescending { message -> message.ts }
        liveMessages.postValue(tempList)
    }

    fun msgEmpty(): Boolean {
        return liveMessages.value.isNullOrEmpty()
    }

    fun hasSelectThings(): Boolean {
        val list = liveMessages.value
        list?.run {
            for (message in this) {
                if (message.selected) {
                    return true
                }
            }
        }
        return false
    }

    fun falseSelectModel() {
        selectModel = false
        unSelectAll()
    }

    fun selectAll() {
        val list = liveMessages.value
        list?.run {
            for (message in this) {
                message.selected = true
            }
        }
    }

    fun unSelectAll() {
        val list = liveMessages.value
        list?.run {
            for (message in this) {
                message.selected = false
            }
        }
    }
}

class MessageActivityRep : ViewModel() {

    fun getMessage(liveMessages: MutableLiveData<List<Message>>) {
        val call = ApiClientBuilder.createApiClient().getMessages()
        call.enqueue(object : Callback<PXResponse<Messages>> {
            override fun onResponse(
                call: Call<PXResponse<Messages>>,
                response: Response<PXResponse<Messages>>
            ) {
                if (response.isSuccessful) {
                    Log.i(
                        "MessageActivityRep",
                        " messages:${Utility.convertGsonToString(response.body())}"
                    )
                    val messages =
                        response.body()?.result?.messages?.sortedByDescending { message -> message.ts }
                    liveMessages.postValue(messages)
                } else {
                    liveMessages.postValue(null)
                }
            }

            override fun onFailure(call: Call<PXResponse<Messages>>, t: Throwable) {
                liveMessages.postValue(null)
            }
        })
    }
}