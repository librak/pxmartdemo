package com.homework.pxmartdemo.ui.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.homework.pxmartdemo.R
import com.homework.pxmartdemo.api.gson.Banner


class BannerAdapter(private val ctx: Context, private val imgs: List<Banner>) : PagerAdapter() {

    override fun getCount(): Int {
       return imgs.size
    }

    override fun isViewFromObject(view: View, obj: Any) = view === obj


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = LayoutInflater.from(ctx).inflate(R.layout.item_banner, container, false)
        val ivImg: ImageView = itemView.findViewById(R.id.iv_banner)
        Glide.with(ctx).load(imgs[position].image).into(ivImg)
        ivImg.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(imgs[position].target_url))
            ctx.startActivity(browserIntent)
        }
        container.addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as? View)
    }
}