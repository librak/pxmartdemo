package com.homework.pxmartdemo.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Rect
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.WindowInsets
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.homework.pxmartdemo.R
import com.homework.pxmartdemo.Utility
import com.homework.pxmartdemo.api.ApiClientBuilder
import com.homework.pxmartdemo.api.PXResponse
import com.homework.pxmartdemo.api.gson.Banners
import com.homework.pxmartdemo.databinding.ActivityMainBinding
import com.homework.pxmartdemo.ui.adapter.BannerAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.await


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainActivityViewModel
    private var screenWidth = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()
        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        screenWidth = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val metrics = windowManager.currentWindowMetrics
            val insets = metrics.windowInsets.getInsetsIgnoringVisibility(WindowInsets.Type.systemBars())
            (metrics.bounds.width() - insets.left - insets.right)
        } else {
            val screenSize = Rect()
            @Suppress("DEPRECATION")
            windowManager?.defaultDisplay?.getRectSize(screenSize)
            screenSize.width()
        }

        setActionBar()
        setView()

        viewModel.httpBanners()
    }

    @SuppressLint("InflateParams")
    private fun setActionBar() {
        setSupportActionBar(binding.toolbar)
        val customView =
            LayoutInflater.from(this).inflate(R.layout.actionbar_img_title_msg, null)
        val params = ActionBar.LayoutParams(
            ActionBar.LayoutParams.MATCH_PARENT,
            ActionBar.LayoutParams.WRAP_CONTENT,
            Gravity.START or Gravity.CENTER_VERTICAL
        )
        supportActionBar?.run {
            setCustomView(customView, params)
            setDisplayHomeAsUpEnabled(false)
            setDisplayShowHomeEnabled(false)
            setDisplayShowCustomEnabled(true)
            val tvTitle = customView.findViewById<TextView?>(R.id.tv_title)
            tvTitle?.text = getString(R.string.app_name)
            val btMsg = customView.findViewById<ImageButton?>(R.id.btn_msg)
            btMsg?.setOnClickListener {
                val intent = Intent(this@MainActivity, MessageActivity::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.right_in, R.anim.hold)
            }
        }
    }

    private fun setView() {
        viewModel.getLiveBanners().observe(this, {
            it.banners?.let { list ->
                val pager = binding.pagerBanner
                pager.offscreenPageLimit = 3
                pager.pageMargin = 20
                pager.adapter = BannerAdapter(this, list)
                binding.tabLayout.setupWithViewPager(pager)
                startAutoScroll(pager)
            }
        })

        viewModel.getLiveHttpMsg().observe(this, {

        })

        binding.btPay.setOnClickListener {
            val intent = Intent(this, QRCodeActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.right_in, R.anim.hold)
        }
    }

    private fun startAutoScroll(pager: ViewPager) {
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed(object : Runnable {
            override fun run() {
                pager.adapter?.count?.let { count ->
                    pager.currentItem.let { pos ->
                        var nextPage = pos + 1
                        if (pos == (count - 1)) {
                            nextPage = 0
                        }
                        pager.setCurrentItem(nextPage, true)
                    }
                }
                handler.postDelayed(this, 3000)
            }
        }, 3000)
    }

}

class MainActivityViewModel : ViewModel() {
    private val rep = MainActivityRep()
    private var liveHttpMsg: MutableLiveData<String> = MutableLiveData()
    private var liveBanners: MutableLiveData<Banners> = MutableLiveData()

    fun getLiveHttpMsg(): LiveData<String> {
        return liveHttpMsg
    }

    fun getLiveBanners(): LiveData<Banners> {
        return liveBanners
    }

    fun httpBanners() {
        rep.getBanners(liveBanners)
    }

}

class MainActivityRep {
    fun getBanners(liveBanners: MutableLiveData<Banners>) {
        val call = ApiClientBuilder.createApiClient().getBanners()
        call.enqueue(object : Callback<PXResponse<Banners>> {
            override fun onResponse(
                call: Call<PXResponse<Banners>>,
                response: Response<PXResponse<Banners>>
            ) {
                if (response.isSuccessful) {
                    Log.i(
                        "MainActivityRep",
                        " banners:${Utility.convertGsonToString(response.body())}"
                    )
                    liveBanners.postValue(response.body()?.result)
                }
            }

            override fun onFailure(call: Call<PXResponse<Banners>>, t: Throwable) {

            }

        })
    }

    fun getMessage() {
        CoroutineScope(Dispatchers.IO).launch {
            val result = ApiClientBuilder.createApiClient().getMessages().await()
            Log.e("MainActivityRep", " messages:${Utility.convertGsonToString(result)}")
        }
    }

    fun getPayment() {
        CoroutineScope(Dispatchers.IO).launch {
            val result = ApiClientBuilder.createApiClient().getPayment().await()
            Log.e("MainActivityRep", " qrcode:${Utility.convertGsonToString(result)}")
        }
    }
}